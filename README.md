# Домашнее задание
Текст скрипта:
```
#!/bin/bash
lockfile=/run/script.pid
begin=`date`
function mysendmail() {
        cat /dev/null > mymail
        echo "ОБРАБАТЫВАЕМЫЙ ИНТЕРВАЛ: $begin - `date`" >> mymail
        echo "Наиболее частые IP-адреса: " >> mymail
        cat myaccess.log | awk '{print $1}'  | sort | uniq -c | sort -k 1 -nr | head -n $2 >> mymail
        echo "Наиболее часто запрашиваемые адреса: " >> mymail
        cat myaccess.log | sed -nE "/GET|POST|HEAD|PROPFIND/p" | awk '{print $7}'  | sort | uniq -c | sort -k 1 -nr | head -n $3 >> mymail
        echo "Все ошибки: " >> mymail
        cat myerror.log >> mymail
        echo "Список всех кодов возврата: " >> mymail
        cat myaccess.log | awk -F \" '{print $3}' | awk '{print $1}'  | sort | uniq -c | sort -k 1 -nr >> mymail
        sendmail $1 < mymail
}
if ( set -o noclobber; echo "$$" > "$lockfile") 2> /dev/null;
then
 trap 'find / -path "$lockfile" -delete; trap - SIGTERM && kill -- -$$; exit $?' INT TERM EXIT KILL
 cat /dev/null > myaccess.log
 cat /dev/null > myerror.log
 tail -n0 -f /var/log/nginx/access.log >> myaccess.log &
 tail -n0 -f /var/log/nginx/error.log >> myerror.log &
 while true
 do
 sleep 3600 &
 wait $!
 mysendmail "$@"
 done
 find / -path "$lockfile" -delete
 trap - INT TERM EXIT
else
 echo "Failed to acquire lockfile: $lockfile."
 echo "Held by $(cat $lockfile)"
fi
```

Пример использования: 
```
 ./script.sh root@localhost 10 10 &
```